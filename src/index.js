const fs = require("fs");
var https = require("https");
const axios = require("axios");
const path = require("path");

// declare TLS protocol
require("tls").DEFAULT_MIN_VERSION = "TLSv1";
const url = "https://18.141.110.57:50000/b1s/v1";
const credentials = {
  CompanyDB: "DEVBFI_AMS",
  B1SESSION: "",
  ROUTEID: ".node1",
};

// login
const login = async () => {
  try {
    const res = await axios({
      method: "POST",
      url: `${url}/Login`,
      data: {
        CompanyDB: credentials.CompanyDB,
        Password: "1234",
        UserName: "BFI009",
      },
      headers: {
        "Content-Type": "application/json",
      },
      httpsAgent: new https.Agent({ rejectUnauthorized: false }),
    });
    credentials.B1SESSION = res.data.SessionId;

    // await modules();
    // await delay(5000);

    // await roles();
    // await delay(5000);

    // await actions();
    // await delay(5000);

    // await accessRights();
    // await delay(5000);

    // await activity_logs();
    // await delay(5000);

    // await users();
    // await delay(5000);

    // await asset_master();
    // await delay(5000);

    // await assignment_header();
    // await delay(5000);

    // await assignment_details();
    // await delay(5000);

    // await assignment_udo_registration();
    // await delay(5000);

    // await assignment_status();
    // await delay(5000);

    // await issuance_header();
    // await delay(5000);

    // await issuance_details();
    // await delay(5000);

    // await issuance_udo_registration();
    // await delay(5000);

    // await issuance_status();
    // await delay(5000);

    // await retirement_header();
    // await delay(5000);

    // await retirement_details();
    // await delay(5000);

    // await retirement_udo_registration();
    // await delay(5000);

    // await retirement_status();
    // await delay(5000);

    // await transfer_header();
    // await delay(5000);

    // await transfer_details();
    // await delay(5000);

    // await transfer_udo_registration();
    // await delay(5000);

    // await transfer_status();
    // await delay(5000);

    // await item_conditions();
    // await delay(5000);

    // await location_header();
    // await delay(5000);

    // await location_details();
    // await delay(5000);

    // await location_udo_registration();
    // await delay(5000);

    // await retirement_reasons();
    // await delay(5000);

    // await store_header();
    // await delay(5000);

    // await store_details();
    // await delay(5000);

    // await store_udo_registration();
    // await delay(10000);

    // /**DATA */

    // await modulesData();
    // await delay(5000);

    // await rolesData();
    // await delay(5000);

    // await actionsData();
    // await delay(5000);

    // await accessData();
    // await delay(5000);

    // await usersData();
    // await delay(5000);

    // await assignment_status_data();
    // await delay(5000);

    // await issuance_status_data();
    // await delay(5000);

    // await retirement_status_data();
    // await delay(5000);

    // await transfer_status_data();
    // await delay(5000);
  } catch (e) {
    console.log("Error login: ", e);
  }
};

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/******************** DATA **********/

const usersData = async () => {
  // const employee = await axios({
  //   method: "POST",
  //   url: `${url}/EmployeesInfo`,
  //   headers: {
  //     "Content-Type": "multipart/mixed;boundary=a",
  //     Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
  //   },
  //   data: {
  //     FirstName: "Trevor",
  //     MiddleName: "Grand",
  //     LastName: "Bach",
  //   },
  //   httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  // })
  //   .then((res) => res.data)
  //   .catch((err) => console.log(err.response.data));
  // console.log(employee);

  // await delay(5000);
  await axios({
    method: "POST",
    url: `${url}/U_USERS`,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    data: {
      Code: "1",
      Name: "1",
      U_EMPLOYEE_CODE: 392,
      // U_EMPLOYEE_CODE: employee.EmployeeID,
      U_USERNAME: "Admin",
      U_PASSWORD: "1234",
      U_COMPANY_CODE: 9251,
      U_ROLE_CODE: "1",
      U_IS_ACTIVE: 1,
      U_IS_SAP_USER: 0,
      U_CREATED_AT: "2020-01-17",
      U_CREATED_TIME: "10:05",
      U_CREATED_BY: "1",
      U_UPDATED_AT: "2020-01-17",
      U_UPDATED_TIME: "10:05",
      U_UPDATED_BY: "1",
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const rolesData = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/roles_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const modulesData = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/modules_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const actionsData = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/actions_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const accessData = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/access_rights_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const assignment_status_data = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/assignment_status_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const issuance_status_data = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/issuance_status_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const retirement_status_data = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/retirement_status_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const transfer_status_data = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/data_batch_request/system/transfer_status_data.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

/******************** TABLES **********/

//Access_rights
const accessRights = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/access_rights.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//actions
const actions = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(__dirname + "/batch_request/system/actions.txt");

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Transactions
const modules = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(__dirname + "/batch_request/system/modules.txt");

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

const roles = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(__dirname + "/batch_request/system/roles.txt");

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//user
const users = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(__dirname + "/batch_request/system/users.txt");

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};
//Access_rights
const activity_logs = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/activity_logs.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Asset Master
const asset_master = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/asset_master.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Assignment Header
const assignment_header = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/assignment_header.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Assignment Details
const assignment_details = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/assignment_details.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Assignment UDO
const assignment_udo_registration = async () => {
  await axios({
    method: "POST",
    url: `${url}/UserObjectsMD`,
    data: {
      Code: "ASSIGNMENTS",
      Name: "Assignments",
      TableName: "ASSIGNMENT_HEADER",
      ObjectType: "boud_Document",
      UserObjectMD_ChildTables: [
        {
          TableName: "ASSIGNMENT_DETAILS",
          ObjectName: "ASSIGNMENT_DETAILS",
          SonNumber: "1",
        },
      ],
    },
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Assignment Status
const assignment_status = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/assignment_status.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Issuance Header
const issuance_header = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/issuance_header.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Issuance Details
const issuance_details = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/issuance_details.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Issuance UDO
const issuance_udo_registration = async () => {
  await axios({
    method: "POST",
    url: `${url}/UserObjectsMD`,
    data: {
      Code: "ISSUANCES",
      Name: "Issuances",
      TableName: "ISSUANCE_HEADER",
      ObjectType: "boud_Document",
      UserObjectMD_ChildTables: [
        {
          TableName: "ISSUANCE_DETAILS",
          ObjectName: "ISSUANCE_DETAILS",
          SonNumber: "1",
        },
      ],
    },
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Issuance Status
const issuance_status = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/issuance_status.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Retirement Header
const retirement_header = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/retirement_header.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Retirement Details
const retirement_details = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/retirement_details.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Retirement UDO
const retirement_udo_registration = async () => {
  await axios({
    method: "POST",
    url: `${url}/UserObjectsMD`,
    data: {
      Code: "RETIREMENTS",
      Name: "Retirements",
      TableName: "RETIREMENT_HEADER",
      ObjectType: "boud_Document",
      UserObjectMD_ChildTables: [
        {
          TableName: "RETIREMENT_DETAILS",
          ObjectName: "RETIREMENT_DETAILS",
          SonNumber: "1",
        },
      ],
    },
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Issuance Status
const retirement_status = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/retirement_status.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Transfer Header
const transfer_header = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/transfer_header.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Transfer Details
const transfer_details = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/transfer_details.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Transfer UDO
const transfer_udo_registration = async () => {
  await axios({
    method: "POST",
    url: `${url}/UserObjectsMD`,
    data: {
      Code: "TRANSFERS",
      Name: "Transfers",
      TableName: "TRANSFER_HEADER",
      ObjectType: "boud_Document",
      UserObjectMD_ChildTables: [
        {
          TableName: "TRANSFER_DETAILS",
          ObjectName: "TRANSFER_DETAILS",
          SonNumber: "1",
        },
      ],
    },
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Transfer Status
const transfer_status = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/transfer_status.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Item Conditions
const item_conditions = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/item_conditions.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Locaiton Header
const location_header = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/location_header.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Location Details
const location_details = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/location_details.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Location UDO
const location_udo_registration = async () => {
  await axios({
    method: "POST",
    url: `${url}/UserObjectsMD`,
    data: {
      Code: "LOCATIONS",
      Name: "Locations",
      TableName: "LOCATION_HEADER",
      ObjectType: "boud_Document",
      UserObjectMD_ChildTables: [
        {
          TableName: "LOCATION_DETAILS",
          ObjectName: "LOCATION_DETAILS",
          SonNumber: "1",
        },
      ],
    },
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Retirement Reasons
const retirement_reasons = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/retirement_reasons.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Store Header
const store_header = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/store_header.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Store Details
const store_details = async () => {
  //     console.log(credentials)
  const body = fs.readFileSync(
    __dirname + "/batch_request/system/store_details.txt"
  );

  await axios({
    method: "POST",
    url: `${url}/$batch`,
    data: body,
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

//Store UDO
const store_udo_registration = async () => {
  await axios({
    method: "POST",
    url: `${url}/UserObjectsMD`,
    data: {
      Code: "STORES",
      Name: "Stores",
      TableName: "STORE_HEADER",
      ObjectType: "boud_Document",
      UserObjectMD_ChildTables: [
        {
          TableName: "STORE_DETAILS",
          ObjectName: "STORE_DETAILS",
          SonNumber: "1",
        },
      ],
    },
    headers: {
      "Content-Type": "multipart/mixed;boundary=a",
      Cookie: `CompanyDB=${credentials.CompanyDB};B1SESSION=${credentials.B1SESSION};ROUTEID=${credentials.ROUTEID}`,
    },
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  })
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err.response.data));
};

login();
