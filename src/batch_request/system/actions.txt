--a

Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
"TableName": "ACTIONS",
"TableDescription": "Actions",
"TableType": "bott_NoObject"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ACTION_NAME",
"Type": "db_Alpha",
"Size": 50,
"Description": "Action Name",
"TableName": "@ACTIONS",
"Mandatory": "tYES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "MODULE_CODE",
"Type": "db_Alpha",
"Size": 20,
"Description": "Module Code",
"TableName": "@ACTIONS",
"Mandatory": "tYES",
"LinkedTable":"MODULES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "IS_ACTIVE",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Status",
"TableName": "@ACTIONS",
"Mandatory": "tYES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_AT",
"Type": "db_Date",
"Description": "Created At",
"TableName": "@ACTIONS"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_TIME",
"Type": "db_Date",
"Description": "Created Time",
"SubType": "st_Time",
"TableName": "@ACTIONS"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Created By",
"TableName": "@ACTIONS"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_AT",
"Type": "db_Date",
"Description": "Updated At",
"TableName": "@ACTIONS"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_TIME",
"Type": "db_Date",
"Description": "Updated Time",
"SubType": "st_Time",
"TableName": "@ACTIONS"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Updated By",
"TableName": "@ACTIONS"
}

--b--
--a--