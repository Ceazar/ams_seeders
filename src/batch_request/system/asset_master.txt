--a

Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
"TableName": "ASSET_MASTER",
"TableDescription": "Asset Master",
"TableType": "bott_NoObject"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ASSET_CODE",
"Type": "db_Alpha",
"Size": 50,
"Description": "Asset Code",
"TableName": "@ASSET_MASTER",
"Mandatory": "tYES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "DESCRIPTION",
"Type": "db_Alpha",
"Size": 100,
"Description": "Description",
"TableName": "@ASSET_MASTER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CATEGORY",
"Type": "db_Alpha",
"Size": 50,
"Description": "Category",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CAPITALIZATION_CODE",
"Type": "db_Alpha",
"Size": 50,
"Description": "Capitalization Code",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "RECEIVING_DATE",
"Type": "db_Date",
"Description": "Receiving Date",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "FIXED_ASSET_COST",
"Type": "db_Numeric",
"EditSize": 11,
"Description": "Fixed Asset Cost",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "INVENTORY_COST",
"Type": "db_Alpha",
"EditSize": 11,
"Description": "Inventory Cost",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "STORE_ID",
"Type": "db_Alpha",
"Size": 50,
"Description": "Store ID",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "GROUP_ID",
"Type": "db_Alpha",
"Size": 50,
"Description": "Group ID",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "PRIMARY_ACCNTABLE",
"Type": "db_Alpha",
"Size": 50,
"Description": "Primary Accountable",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "SECONDARY_ACCNTABLE",
"Type": "db_Alpha",
"Size": 50,
"Description": "Secondary Accountable",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "APPROVER",
"Type": "db_Alpha",
"Size": 50,
"Description": "Approver Id",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "REMARKS",
"Type": "db_Memo",
"Description": "Remarks",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "IS_ISSUED",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Issued?",
"TableName": "@ASSET_MASTER",
"Mandatory": "tYES",
"DefaultValue": "0"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "SERIAL_NO",
"Type": "db_Alpha",
"Size": 50,
"Description": "Serial Number",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "MODEL_NO",
"Type": "db_Alpha",
"Size": 50,
"Description": "Model Number",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CONDITION",
"Type": "db_Alpha",
"Size": 50,
"Description": "Condition",
"TableName": "@ASSET_MASTER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "IS_ASSIGNED",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Assigned?",
"TableName": "@ASSET_MASTER",
"Mandatory": "tYES",
"DefaultValue": "0"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ASSIGNED_TO",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Assigned To",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ASSIGN_START",
"Type": "db_Date",
"Description": "Assignment Start Date",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ASSIGN_END",
"Type": "db_Date",
"Description": "Assignment End Date",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "IS_RETIRED",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Retired?",
"TableName": "@ASSET_MASTER",
"Mandatory": "tYES",
"DefaultValue": "0"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "REASON_CODE",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Retirement Reason Code",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "REQUEST_TO",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Retirement Request To",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ATTACHMENT_ENTRY",
"Type": "db_Numeric",
"EditSize": 11,
"Description": "Attachment Entry",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "MAIN_LOC",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Main Location",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "SUB_LOC",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Sub Location",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "IS_ACTIVE",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Status",
"TableName": "@ASSET_MASTER",
"Mandatory": "tYES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "TAG_NUMBER",
"Type": "db_Alpha",
"EditSize": 100,
"Description": "Tag Number",
"TableName": "@ASSET_MASTER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_AT",
"Type": "db_Date",
"Description": "Created At",
"TableName": "@ASSET_MASTER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_TIME",
"Type": "db_Date",
"Description": "Created Time",
"SubType": "st_Time",
"TableName": "@ASSET_MASTER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Created By",
"TableName": "@ASSET_MASTER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_AT",
"Type": "db_Date",
"Description": "Updated At",
"TableName": "@ASSET_MASTER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_TIME",
"Type": "db_Date",
"Description": "Updated Time",
"SubType": "st_Time",
"TableName": "@ASSET_MASTER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Updated By",
"TableName": "@ASSET_MASTER"
}

--b--
--a--