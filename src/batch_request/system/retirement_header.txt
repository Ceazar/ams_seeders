--a

Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
"TableName": "RETIREMENT_HEADER",
"TableDescription": "Retirement Header",
"TableType": "bott_Document"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "STORE_ID",
"Type": "db_Alpha",
"Size": 50,
"Description": "Store ID",
"TableName": "@RETIREMENT_HEADER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "GROUP_ID",
"Type": "db_Alpha",
"Size": 50,
"Description": "Group ID",
"TableName": "@RETIREMENT_HEADER",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "REQUEST_TO",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Retirement Request To",
"TableName": "@RETIREMENT_HEADER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ATTACHMENT_ENTRY",
"Type": "db_Numeric",
"EditSize": 11,
"Description": "Attachment Entry",
"TableName": "@RETIREMENT_HEADER",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "STATUS",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Status",
"TableName": "@RETIREMENT_HEADER",
"Mandatory": "tYES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_AT",
"Type": "db_Date",
"Description": "Created At",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_TIME",
"Type": "db_Date",
"Description": "Created Time",
"SubType": "st_Time",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CREATED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Created By",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "APPROVED_AT",
"Type": "db_Date",
"Description": "Approved At",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "APPROVED_TIME",
"Type": "db_Date",
"Description": "Approved Time",
"SubType": "st_Time",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "APPROVED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Approved By",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "RETIRED_AT",
"Type": "db_Date",
"Description": "Retired At",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "RETIRED_TIME",
"Type": "db_Date",
"Description": "Retired Time",
"SubType": "st_Time",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "RETIRED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Retired By", 
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_AT",
"Type": "db_Date",
"Description": "Updated At",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_TIME",
"Type": "db_Date",
"Description": "Updated Time",
"SubType": "st_Time",
"TableName": "@RETIREMENT_HEADER"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "UPDATED_BY",
"Type": "db_Alpha",
"Size": 10,
"Description": "Updated By",
"TableName": "@RETIREMENT_HEADER"
}

--b--
--a--