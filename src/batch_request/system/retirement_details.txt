--a

Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
"TableName": "RETIREMENT_DETAILS",
"TableDescription": "Retirement Details",
"TableType": "bott_DocumentLines"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "ASSET_CODE",
"Type": "db_Alpha",
"Size": 50,
"Description": "Asset Code",
"TableName": "@RETIREMENT_DETAILS",
"Mandatory": "tYES"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "DESCRIPTION",
"Type": "db_Alpha",
"Size": 100,
"Description": "Description",
"TableName": "@RETIREMENT_DETAILS"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "CATEGORY",
"Type": "db_Alpha",
"Size": 50,
"Description": "Category",
"TableName": "@RETIREMENT_DETAILS",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "STORE_ID",
"Type": "db_Alpha",
"Size": 50,
"Description": "Store ID",
"TableName": "@RETIREMENT_DETAILS",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "GROUP_ID",
"Type": "db_Alpha",
"Size": 50,
"Description": "Group ID",
"TableName": "@RETIREMENT_DETAILS",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "REASON_CODE",
"Type": "db_Alpha",
"EditSize": 50,
"Description": "Retirement Reason Code",
"TableName": "@RETIREMENT_DETAILS",
"Mandatory": "tNO",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "REMARKS",
"Type": "db_Memo",
"Description": "Remarks",
"TableName": "@RETIREMENT_DETAILS",
}

--b--
--a--