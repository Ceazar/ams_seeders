--a

Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
"TableName": "STORE_DETAILS",
"TableDescription": "Store Groups",
"TableType": "bott_DocumentLines"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "GROUP_CODE",
"Type": "db_Alpha",
"Size": 50,
"Description": "Group Code",
"TableName": "@STORE_DETAILS",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "GROUP_NAME",
"Type": "db_Alpha",
"Size": 100,
"Description": "Group Name",
"TableName": "@STORE_DETAILS",
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "IS_ACTIVE",
"Type": "db_Numeric",
"EditSize": 1,
"Description": "Status",
"TableName": "@STORE_DETAILS",
"Mandatory": "tYES"
}

--b--
--a--