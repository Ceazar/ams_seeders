--a

Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"1",
    "Name":"1",
	"U_ACTION_NAME":"View modules",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"2",
    "Name":"2",
	"U_ACTION_NAME":"Add module",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"3",
    "Name":"3",
	"U_ACTION_NAME":"Edit module",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"4",
    "Name":"4",
	"U_ACTION_NAME":"View actions",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"5",
    "Name":"5",
	"U_ACTION_NAME":"Add action",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"6",
    "Name":"6",
	"U_ACTION_NAME":"Edit action",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"7",
    "Name":"7",
	"U_ACTION_NAME":"View roles and access rights",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"8",
    "Name":"8",
	"U_ACTION_NAME":"Add role and access rights",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"9",
    "Name":"9",
	"U_ACTION_NAME":"Edit role and access rights",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"10",
    "Name":"10",
	"U_ACTION_NAME":"View users",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"11",
    "Name":"11",
	"U_ACTION_NAME":"Add user",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"12",
    "Name":"12",
	"U_ACTION_NAME":"Edit user",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"13",
    "Name":"13",
	"U_ACTION_NAME":"View activity logs",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"14",
    "Name":"14",
	"U_ACTION_NAME":"Reset user password",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_ACTIONS

{
    "Code":"15",
    "Name":"15",
	"U_ACTION_NAME":"Update SAP user",
	"U_MODULE_CODE":"1",
	"U_IS_ACTIVE":1,
	"U_CREATED_AT":"2020-01-17",
	"U_CREATED_TIME":"10:05",
	"U_CREATED_BY":"1",
	"U_UPDATED_AT":"2020-01-17",
	"U_UPDATED_TIME":"10:05",
	"U_UPDATED_BY":"1"
}

--b--
--a--